# sshquare device

Install this small script on the machine you wish to connect to. Basically it
creates a small init script that creates a tunnel to the sshquare.io servers and
allows you to gain ssh access from virtually everywhere.

## Setup

```
cd /tmp
wget "https://gitlab.com/juanitobananas/sshquare-device/raw/master/install-ubuntu-debian"
chmod +x /tmp/install-ubuntu-debian
sudo /tmp/install-ubuntu-debian
rm /tmp/install-ubuntu-debian
```
